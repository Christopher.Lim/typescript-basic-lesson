// type date object
const today = new Date();
console.log(today.getMonth());

// interface
const person = {
  age: 20
};
console.log(person.age);

// class
class Color {
  red: 'red'
}
//accessing the class
const color = new Color();
console.log(color.red);