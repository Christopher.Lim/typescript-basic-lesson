const profile = {
  name: 'alex',
  age: 20,
  coords: {
    lat: 0,
    lng: 15
  },
  setAge(age: number) {
    this.age = age;
  }
};

// uses destructuring
// object - age property - number
const { age }: { age: number } = profile;

// multiple object destructuring
const { 
  coords: { lat, lng} 
}: { coords: { lat: number; lng: number } } = profile;



