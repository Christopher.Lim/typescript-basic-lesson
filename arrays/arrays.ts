// avoid any 
// add annotation if you did not initialize an array
// okey not to add anotation if you initialize the array
const carMakers: string[] = ['ford', 'toyota', 'chevy'];
const dates = [new Date(), new Date()];

// two dimensional array
// carsByMaker: string[][]
const carsByMake = [
  ['f150'],
  ['corolla'],
  ['camaro']
];


// Help with inference when extracting values
const car = carMakers[0];
const myCar = carMakers.pop();

// Prevent incompatible values
// carMakers.push(100);

// Help with 'map'
//auto complete
carMakers.map((car: string): string => {
  return car.toUpperCase();
});

// Flexible types
const importantDates = [new Date(), '2030-10-10'];
const importantDatesTwo: (Date | string)[] = [new Date()];
importantDates.push('2030-10-10');
importantDates.push(new Date());
// importantDates.push(100); // Causes an error because its not a string or a date


