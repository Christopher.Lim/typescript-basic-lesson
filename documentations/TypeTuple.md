### Type Tuple
- Array-like structure where each element represents some property of a record
<br >

```
// Object representing a 'drink
color => brown (string)
carbonated => true (boolean)
sugar => 40 (number)
```
<br>

```
array example
[blue, true, 40 ]
- we lost the property or information
-If we work with tuple its going to have fix information
```

### 