### Type Annotations and Interence
`Type annotations` - Code we add to tell Typescript what type of value a variable will refer to<br>
`Type inference` - Typescript tries to figure out what type of value a vairable refers to <br >
<br><br>
In short Type annotations is we developers tell Typescript the type, while Type inference is Typescript guesses the type.
<br><br>

#### Type annotations for functions
- Code we add to tell Typescript what type of arguments
a function will receive and what type of values it will return

